; x/5 + y*2 - 3*z ;
;edi, esi, edx;

format ELF64

section '.text' executable
public func
func:
		mov eax, edi 
		mov ecx, edx 	
		mov edi, esi 
		mov edx, 0
		CDQ
		mov esi, 5
		idiv esi

		mov esi, eax
		mov eax, edi
		mov edx, 2
		imul edx
		add esi, eax
		mov eax, esi

		mov esi, eax
		mov eax, ecx
		mov edx, 3
		imul edx
		sub esi, eax
		mov eax, esi
	ret
