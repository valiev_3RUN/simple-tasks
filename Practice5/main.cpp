#include <iostream>
using namespace std;

extern "C" int func(int x, int y, int z);
//x/5 + y*2 - 3*z ;
int main() {

	int x, y, z;
	cout << "x: "; cin >> x;
	cout << "y: "; cin >> y;
	cout << "z: "; cin >> z;

	cout << "Asm: "  << func(x,y,z) << endl;

	int result = x/5 + y*2 - 3*z;
	printf("%d/5 + %d*2 - 3*%d = %d\n", x, y, z, result);
	return 0;
}