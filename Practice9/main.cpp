#include <iostream>
#include <vector>
#include <string>
using namespace std;
static const char*  symbols = "АБВГДЕЖЗИКабвгдежзик";
class Consignment {
protected:
    string name;
public:
    Consignment() {
        cout << "Enter the name consignment: ";
        cin >> name;
    }
    virtual string getName() = 0;
    virtual ~Consignment() {}
};

class ConsignmentBudget : public Consignment {
private:
    size_t sizeAssign;
public:
    ConsignmentBudget() {}
    ConsignmentBudget(size_t sizeAssign) {
        this->sizeAssign = sizeAssign;
    }
    string getName() override {
        return this->name;
    }
    size_t getSizeAssign() {
        return sizeAssign;
    }
    ~ConsignmentBudget() override{}

};

class ConsignmentOther : public Consignment {
private:
    size_t countDeputy;
public:
    ConsignmentOther() {}
    ConsignmentOther(size_t countDeputy) {
        this->countDeputy = countDeputy;
    }
    string getName() override {
        return this->name;
    }
    size_t getCountDeputy() {
        return countDeputy;
    }
    ~ConsignmentOther() override {}
};

void Print(vector<Consignment*>& object) {
    for (size_t i = 0; i < object.size(); ++i) {
     string temp = object[i]->getName();
         for (size_t j = 0; j < sizeof(symbols)/sizeof(symbols[0]); ++i) {
             if (temp[0] == symbols[j]) {
                cout << object[i]->getName() << endl;
                break;
             }
         }
    }
}

int main()
{
    vector<Consignment*> obj;
    do {

        cout << "Enter the type consignment: :" << endl;
        cout << "1 - Budget\n";
        cout << "2 - Other\n";
        char ch;
        cin >> ch;
        if (ch == '1') {
            cout << "Enter the size assignment: ";
            size_t sizeAssignment; cin >> sizeAssignment;
            obj.push_back(new ConsignmentBudget(sizeAssignment));
        } else {
            cout << "Enter the count deputy: ";
            size_t countDeputy; cin >> countDeputy;
            obj.push_back(new ConsignmentOther(countDeputy));
        }
        cout << "Add more? y/n\n";
        cin >> ch;
        if (ch == 'n') break;

    } while (true);

    system("clear");

    cout << "The current list: \n";
    Print(obj);
    return 0;
}
