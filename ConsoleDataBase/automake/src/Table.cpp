#include "./../includes/Table.h"
using namespace std;
void Table::Add(string &date, Type_operation type, size_t amount, string &description, string &correspondent)
{
	this->date = date;
	this->type = type;
	this->amount = amount;
	this->description = description;
	this->correspondent = correspondent;
}



bool Table::Delete(const size_t& number, vector<Table>& vec)
{
	if (number < vec.size()) {
		vec.erase(vec.begin() + number);
		return true;
	}
	else return false;
}
Table&  Table::getObject()
{
	static Table object;
	return object;
}


string Table::getDate()
{
	return this->date;
}
string Table::getCorrespondent()
{
	return this->correspondent;
}
string Table::getDescription()
{
	return this->description;
}

size_t Table::getAmount()
{
	return this->amount;
}
Table::Type_operation Table::getType()
{
	return this->type;
}
string Table::getTypeToStr() {
	return Table::getType() == Table::income ? "income" : "expense";
}
