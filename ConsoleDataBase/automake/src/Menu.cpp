#include "./../includes/Menu.h"
Menu::Menu()
{

}

Menu& Menu::getObject()
{
	static Menu object;
	return object;
}

void Menu::inputData()
{
	string date, description, correspondent;
	Table::Type_operation type;
	char ch;
	size_t amount;
	cout << "Enter the date: "; cin >> date;
	cout << "Enter the type operation (1 - income, 2 - expense): "; cin >> ch;
	ch == '1' ? type = Table::income : type = Table::expense;
	cout << "Enter the amount: "; cin >> amount;
	cout << "Enter the description: "; cin >>  description;
	cout << "Enter the correspondent: "; cin >> correspondent;
	Table::getObject().Add(date, type, amount, description, correspondent);
	list.push_back(Table::getObject());
	cout << endl << "Date added successfully. Enter for back to main menu.";
	cin.get();
	
	
}

void Menu::outputData()
{
	if (!list.empty()) {
		//cout  << "Date\t|\t"  << "Type\t|\t" << "Amount\t|\t" << "Descr.\t\t|\t" << "Corres-t\t|\n"; 
		for (size_t i = 0; i < list.size(); i++) {
			cout 
			<< list[i].getDate() << "\t\t"
			<< list[i].getTypeToStr() << "\t\t"
			<< list[i].getAmount() << "\t\t"
			<< list[i].getDescription() << "\t\t\t"
			<< list[i].getCorrespondent() << "\t" << "(" << i << ")" << endl;	
		}
	} else cout << "The list is empty.";
	cin.get();
}

void Menu::editData()
{
	if (!list.empty()) {
		outputData();
		cout << "Enter the line number to edit: ";
		int number;
		cin >> number;
		if (number < list.size()) {
			string date, description, correspondent;
			Table::Type_operation type;
			char ch;
			size_t amount;
			cout << "Enter the date: "; cin >> date;
			cout << "Enter the type operation (1 - income, 2 - expense)"; cin >> ch;
			ch == '1' ? type = Table::income : type = Table::expense;
			cout << "Enter the amount: "; cin >> amount;
			cout << "Enter the description: "; cin >>  description;
			cout << "Enter the correspondent: "; cin >> correspondent;
			list[number].Add(date, type, amount, description, correspondent);
			cout << "The line is edited successfully." << endl;
		} else cout << "Error edit, because net takoy tsifri";
	} else cout << "The list is empty.";
	cin.get();

}
void Menu::deleteData()
{
	if (!list.empty()) {
		outputData();
		cout << "Enter the line number to remove: ";
		int number;
		cin >> number;
		if(Table::getObject().Delete((size_t)number, list)) {
			cout << "The line is removed successfully." << endl;
		} else cout << "Error delete.";
	} else cout << "The list is empty.";
	cin.get();
}
void Menu::loadFromFile()
{
	if (!list.empty()) list.clear();
	cout << "Enter the file name to load: ";
	string fileName; cin >> fileName;
	ifstream file(fileName.c_str());
	if (file.is_open()) {
		while (!file.eof()) {
			string date, description, correspondent;
			Table::Type_operation type;
			char ch;
			size_t amount;
			file >> date;
			file >> ch; ch == '1' ? type = Table::income : type = Table::expense; 
			file >> amount;
			file >> description; 
			file >> correspondent;
			if (file.eof()) break;
			Table::getObject().Add(date, type, amount, description, correspondent);
			list.push_back(Table::getObject());
		}
	}
}

void Menu::saveToFile()
{
	if (!list.empty()) {
		cout << "Enter the file name to save: ";
		string fileName; cin >> fileName;
		ofstream file(fileName.c_str());
		for (int i = 0; i < list.size(); i++) {
			file << list[i].getDate() << endl;
			file << list[i].getType() << endl;
			file << list[i].getAmount() << endl;
			file << list[i].getDescription() << endl;
			file << list[i].getCorrespondent() << endl;
		}
	} else cout << "The list is empty.";
	cin.get();
}
