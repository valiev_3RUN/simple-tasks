#pragma once
#include <string>
#include <vector>
#include <string>

class Table 
{
public:

	enum Type_operation
	{
		income,
		expense
	};
	Table() : date(""), description(""), correspondent(""), type((Type_operation)0), amount(0) {};
	static Table& getObject();
	void Add(std::string &date, Type_operation type, size_t amount, std::string &description, std::string &correspondent);
	bool Delete(const size_t& number, std::vector<Table>& vec);
	std::string getDate();
	std::string getDescription();
	std::string getCorrespondent();
	size_t getAmount();
	Type_operation getType();
	std::string getTypeToStr();
	~Table() {}
private:
	std::string date, description, correspondent;
	Type_operation type;
	size_t amount;
};