#include "Menu.h"
#include <iostream>
using namespace std;
void init() {
	char key;
	do {
		clear();
		printf("________________________ENTER AN ACTION..______________________\n\n");
		printf("1) Add information for businessman\n");
		printf("2) Print information for businessman\n");
		printf("3) Edit information for businessman\n");
		printf("4) Delete information for businessman\n");
		printf("5) Load database from file\n");
		printf("6) Save database to file\n");
		printf("7) Exit\n");

		cin >> key;
		switch (key)
		{
		case '1':	clear(); Menu::getObject().inputData(); cin.get(); break;
		case '2':	clear(); Menu::getObject().outputData(); cin.get(); break;
		case '3':	clear(); Menu::getObject().editData();  cin.get();break;
		case '4':	clear(); Menu::getObject().deleteData(); cin.get();break;
		case '5':	clear(); Menu::getObject().loadFromFile();  cin.get();break;
		case '6':	clear(); Menu::getObject().saveToFile();  cin.get();break;
		case '7':  break;
		}
	} while (key != '7');
}

int main() {
	init(); 
	return 0;
}
