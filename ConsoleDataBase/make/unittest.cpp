#include <iostream>
#include "Table.h"
#include <string>
using namespace std;
bool checkInitParameters()
{
	if (
		Table::getObject().getDate() == "" &&
		Table::getObject().getType() == (Table::Type_operation)0 &&
		Table::getObject().getAmount() == 0 &&
		Table::getObject().getDescription() == "" &&
		Table::getObject().getCorrespondent() == ""
	   ) return true; else return false;
}
bool checkAdd(string& date, string& description, string& correspondent, Table::Type_operation type, size_t amount)
{
	bool flag = false;
	Table::getObject().Add(date, type, amount, description, correspondent);
	flag = true;
	return flag;
}
bool checkGetDate(string& date)
{
	if (Table::getObject().getDate() == date)  return true; else return false;
}

bool checkGetDescription(string& description)
{
	if (Table::getObject().getDescription() == description) return true; return false;
}

bool checkGetCorrespondent(string& correspondent)
{
	if (Table::getObject().getCorrespondent() == correspondent) return true; else return false;
}

bool checkGetType(Table::Type_operation type)
{
	if (Table::getObject().getType() == type)  return true; else return false;
}

bool checkGetAmount(size_t amount)
{
	if (Table::getObject().getAmount() == amount) return true; else return false;
}

bool checkDelete(const size_t& number, std::vector<Table>& vec)
{

	if (!vec.empty()) return (Table::getObject().Delete(number, vec));
		else
		{
			string date = "10.10.18",
				   description = "description",
			       correspondent = "correspondent";
			Table::Type_operation type = Table::income;
			size_t amount = 1000;
			Table::getObject().Add(date, type, amount, description, correspondent);
			return (Table::getObject().Delete(number, vec));
		}

}

bool checkDeleteOfNonexistent(const size_t& number, std::vector<Table>& vec)
{
	if (!vec.empty()) return !(Table::getObject().Delete(number, vec));
		else
		{
			string date = "10.10.18",
				   description = "description",
			       correspondent = "correspondent";
			Table::Type_operation type = Table::income;
			size_t amount = 1000;
			Table::getObject().Add(date, type, amount, description, correspondent);
			return !(Table::getObject().Delete(number, vec));
		}
}
bool checkGetTypeToStr()
{
	if (Table::getObject().getType() == Table::income)
	{
		if (Table::getObject().getTypeToStr() == "income") return true;
	} else
		{
			if (Table::getObject().getTypeToStr() == "expense") return true;	
		}
	return false;
}
int main() {
	string date = "10.10.18",
				description = "description",
				correspondent = "correspondent";
	Table::Type_operation type = Table::income;
	size_t amount = 1000;
	vector<Table> vec = {Table::getObject()};

	if (checkInitParameters()) cout << "Test \"checkInitParameters\" passed.\n";
		else cout << "Test \"checkInitParameters\" error.\n";

	if (checkAdd(date, description, correspondent, type, amount)) cout << "Test \"CheckAdd\" passed.\n";
		else cout << "Test \"CheckAdd\" error.\n";

	if (checkGetDate(date)) cout << "Test \"checkGetDate\" passed.\n";
		else cout << "Test \"checkGetDate\" error.\n";

	if (checkGetDescription(description)) cout << "Test \"checkGetDescription\" passed.\n";
		else cout << "Test \"checkGetDescription\" error.\n";

	if (checkGetCorrespondent(correspondent)) cout << "Test \"checkGetCorrespondent\" passed.\n";
		else cout << "Test \"checkGetCorrespondent\" error.\n";

	if (checkGetType(type)) cout << "Test \"checkGetType\" passed.\n";
		else cout << "Test \"checkGetType\" error.\n";

	if (checkGetAmount(amount)) cout << "Test \"checkGetAmount\" passed.\n";
		else cout << "Test \"checkGetAmount\" error.\n";
	
	if( checkDelete(0,vec) ) cout << "Test \"checkDelete\" passed.\n";
		else cout << "Test \"checkDelete\" error.\n";

	if( checkDeleteOfNonexistent(-1,vec) ) cout << "Test \"checkDeleteOfNonexistent\" passed.\n";
		else cout << "Test \"checkDeleteOfNonexistent\" error.\n";

	if( checkGetTypeToStr() ) cout << "Test \"checkGetTypeToStr\" passed.\n";
		else cout << "Test \"checkGetTypeToStr\" error.\n";

	return 0;
}